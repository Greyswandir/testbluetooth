#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    serviceDiscover = 0;
    deviceDiscover = 0;
    localDevice = new QBluetoothLocalDevice(this);
    if(localDevice->isValid()) {
        localDevice->setHostMode(QBluetoothLocalDevice::HostDiscoverable);
        localDevice->powerOn();
        localDevice->setHostMode(QBluetoothLocalDevice::HostDiscoverable);
        qDebug() << "Local Device: " << localDevice->name();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
    delete localDevice;
}

void MainWindow::on_Search1Button_clicked()
{
    if(!serviceDiscover) {
        serviceDiscover = new QBluetoothServiceDiscoveryAgent(this);
        connect(serviceDiscover, SIGNAL(finished()), this, SLOT(serviceDiscoverFinished()));
        serviceDiscover->start();
    }
}

void MainWindow::on_search2Button_clicked()
{
    if(!deviceDiscover) {
        deviceDiscover = new QBluetoothDeviceDiscoveryAgent(this);
        connect(deviceDiscover, SIGNAL(finished()), this, SLOT(deviceDiscoverFinished()));
        deviceDiscover->start();
    }
}

void MainWindow::serviceDiscoverFinished()
{
    qDebug() << "Service search done";
    QList<QBluetoothServiceInfo> info = serviceDiscover->discoveredServices();
    ui->serviceList->clear();
    QString str;
    if(!info.isEmpty()) {
        for(int i = 0; i < info.count(); i++) {
            str = info.at(i).device().name();
            str += QString(" Id(%1) ").arg(info.at(i).ServiceId);
            str += QString(" service(%1) ").arg(info.at(i).serviceName());
            ui->serviceList->addItem(str);
        }
    }
}

void MainWindow::deviceDiscoverFinished()
{
    qDebug() << "Device search done";
    QList<QBluetoothDeviceInfo> info = deviceDiscover->discoveredDevices();
    ui->deviceList->clear();
    QString str;
    if(!info.isEmpty()) {
        for(int i = 0; i < info.count(); i++) {
            str = info.at(i).name();
            str += QString (" Address (") + info.at(i).address().toString() + QString(")");
            ui->deviceList->addItem(str);
        }
    }
}
