#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QBluetoothLocalDevice>
#include <QBluetoothServiceDiscoveryAgent>
#include <QBluetoothDeviceDiscoveryAgent>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_Search1Button_clicked();
    void on_search2Button_clicked();

    void serviceDiscoverFinished();
    void deviceDiscoverFinished();

private:
    Ui::MainWindow *ui;
    QBluetoothLocalDevice *localDevice;
    QBluetoothServiceDiscoveryAgent *serviceDiscover;
    QBluetoothDeviceDiscoveryAgent *deviceDiscover;
};

#endif // MAINWINDOW_H
